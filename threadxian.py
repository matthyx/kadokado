import collections, copy, os, plateau, Queue, sys, threading, time

def do_work(cur_p):
	global best_score, start_time
	moves = cur_p.g.get_all_moves()
	if not len(moves):
		if cur_p.score > best_score:
			with threadlock:
				#if cur_p.score > best_score:
				print time.time() - start_time, cur_p.score, cur_p.moves
				best_score = cur_p.score
		return
	save_b = copy.copy(cur_p.b.b.map)
	best_plateaux = collections.deque()
	best_plateaux_score = 0
	for move in moves:
		(u, v) = move
		new_score = cur_p.b.remove(u, v) + cur_p.b.eval_board()
		cur_p.b.b.map = copy.copy(save_b)
		if best_plateaux_score > new_score:
			continue
		else:
			if new_score > best_plateaux_score:
				best_plateaux = collections.deque()
				best_plateaux_score = new_score
			new_p = copy.deepcopy(cur_p)
			move_score = new_p.b.remove(u, v)
			new_p.g.delete_node(u)
			new_p.g.delete_node(v)
			new_p.score += move_score
			new_p.moves.append((u, v, move_score))
			best_plateaux.append(new_p)
	for good_plateau in best_plateaux:
		plateau_list.put(good_plateau)

def worker():
	while True:
		cur_p = plateau_list.get()
		do_work(cur_p)
		plateau_list.task_done()

try:
	import psyco
	psyco.full()
	print 'psyco optimized'
except ImportError:
	pass

with open(os.path.join(os.getcwd(), 'plateau2.txt')) as file:
	map_str = file.read()
p = plateau.Plateau(map_str)
print 'score theorique:', p.b.eval_board()

best_score = 0
plateau_list = Queue.LifoQueue()
threadlock = threading.Lock()

# allocate pool
for i in range(8):
	t = threading.Thread(target=worker)
	t.daemon = True
	t.start()

# start !
start_time = time.time()
plateau_list.put(p)

# wait for tasks to finish
plateau_list.join()
