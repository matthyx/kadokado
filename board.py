import map

my_cache = {}

class Board:
	def __init__(self, map_str):
		self.empty_stone = 'xxx'
		self.b = map.Map(map_str)
	def eval_board(self):
		global my_cache
		board_string = str(self.b)
		if my_cache.has_key(board_string):
			return my_cache[board_string]
		m3p = {}
		m2p = {}
		m1p = {}
		nbstones = 0
		# group all stones
		for stone in self.b:
			# skip empty
			if stone == self.empty_stone:
				continue
			# make a count
			nbstones += 1
			# identical stones
			if not m3p.has_key(stone):
				m3p[stone] = 0
			m3p[stone] += 1
			# 2 criteria
			for s2p1 in ('.' + stone[1:], stone[:1] + '.' + stone[2:], stone[:2] + '.'):
				if not m2p.has_key(s2p1):
					m2p[s2p1] = 0
				m2p[s2p1] += 1
			# 1 criterion
			for s1p2 in (stone[0] + '..', '.' + stone[1] + '.', '..' + stone[2]):
				if not m1p.has_key(s1p2):
					m1p[s1p2] = 0
				m1p[s1p2] += 1
		# count pairs
		pairs3 = 0
		for group, count in m3p.items():
			pairs3 += count//2
		pairs2 = 0
		for group, count in m2p.items():
			pairs2 += count//2
		pairs1 = 0
		for group, count in m1p.items():
			pairs1 += count//2
		# normalize counts (cannot have more than nbpairs)
		nbpairs = nbstones//2
		nb3 = min(pairs3, nbpairs)
		nb2 = min(pairs2, nbpairs-nb3)
		nb1 = min(pairs1, nbpairs-nb3-nb2)
		nb0 = nbpairs-nb3-nb2-nb1
		# finally the sum
		score = 1000*nb3 + 300*nb2 + 50*nb1 + nb0
		my_cache[board_string] = score
		return score
	def remove(self, u, v):
		common_pts = 0
		u_str = self.b.get(u)
		v_str = self.b.get(v)
		if u_str[0] == v_str[0]:
			common_pts += 1
		if u_str[1] == v_str[1]:
			common_pts += 1
		if u_str[2] == v_str[2]:
			common_pts += 1
		self.b.set(u, self.empty_stone)
		self.b.set(v, self.empty_stone)
		if common_pts == 3:
			return 1000
		elif common_pts == 2:
			return 300
		elif common_pts == 1:
			return 50
		else:
			return 1
