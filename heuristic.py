import sys

with open('plateau2.txt') as file:
	map_str = file.read()

map = []
for row in map_str.splitlines():
	map.append(row.split(' '))

m3p = {}
m2p = {}
m1p = {}
nbstones = 0
for x, row in enumerate(map):
	for y, stone in enumerate(row):
		if stone == 'xxx':
			continue
		nbstones += 1
		if not m3p.has_key(stone):
			m3p[stone] = []
		m3p[stone].append((x, y))
 		for s2p1 in ('.' + stone[1:], stone[:1] + '.' + stone[2:], stone[:2] + '.'):
 			if not m2p.has_key(s2p1):
 				m2p[s2p1] = []
 			m2p[s2p1].append((x, y))
 		for s1p2 in (stone[0] + '..', '.' + stone[1] + '.', '..' + stone[2]):
 			if not m1p.has_key(s1p2):
 				m1p[s1p2] = []
 			m1p[s1p2].append((x, y))

sum3 = 0
for groupe, membres in m3p.items():
 	sum3 += len(membres)//2
	print groupe, membres
print '==>', sum3
sum2 = 0
for groupe, membres in m2p.items():
	sum2 += len(membres)//2
	print groupe, membres
print '==>', sum2
sum1 = 0
for groupe, membres in m1p.items():
	sum1 += len(membres)//2
	print groupe, membres
print '==>', sum1

nbpairs = nbstones//2
nb3 = min(sum3, nbpairs)
nb2 = min(sum2, nbpairs-nb3)
nb1 = min(sum1, nbpairs-nb3-nb2)
nb0 = nbpairs-nb3-nb2-nb1

print 'score (%s paires): %s %s %s %s -> %s' % (nbpairs, nb3, nb2, nb1, nb0, 1000*nb3 + 300*nb2 + 50*nb1 + nb0)
