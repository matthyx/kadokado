class Graph:
	def __init__(self, map_str):
		self.g = {}
		self.fill_graph(self.parse_map(map_str))
		self.solve_path = {	'l': {'l':'l', 'v':'lv', 'lv':'lv', 'vl':'', },
							'v': {'l':'vl', 'v':'v', 'lv':'', 'vl':'vl', },
							'lv': {'l':'', 'v':'lv', 'lv':'', 'vl':'', },
							'vl': {'l':'vl', 'v':'', 'lv':'', 'vl':'', } }
	def add_node(self, u):
		if not self.g.has_key(u):
			self.g[u] = {}
	def add_path(self, u, v, w):
		if u!=v:
			if not self.g.has_key(u):
				self.add_node(u)
			if not self.g[u].has_key(v):
				self.g[u][v] = w
	def delete_node(self, v):
		for a in self.get_neighbours(v):
			for b in self.get_neighbours(v):
				self.merge_paths(a, b, v)
			del(self.g[a][v])
		del(self.g[v])
	def fill_graph(self, map):
		for y in range(self.nb_columns):
			for x in range(self.nb_rows):
				if self.is_inside(x-1, y):
					self.add_path(map[x][y], map[x-1][y], 'v')
					self.add_path(map[x-1][y], map[x][y], 'v')
				if self.is_inside(x+1, y):
					self.add_path(map[x][y], map[x+1][y], 'v')
					self.add_path(map[x+1][y], map[x][y], 'v')
				if self.is_inside(x, y-1):
					self.add_path(map[x][y], map[x][y-1], 'l')
					self.add_path(map[x][y-1], map[x][y], 'l')
				if self.is_inside(x, y+1):
					self.add_path(map[x][y], map[x][y+1], 'l')
					self.add_path(map[x][y+1], map[x][y], 'l')
	def get_all_moves(self):
		all_moves = set()
		for node in self.get_all_nodes():
			for peer in self.get_neighbours(node):
				all_moves.add(frozenset((node, peer)))
		return all_moves
	def get_all_nodes(self):
		return self.g.keys()
	def get_neighbours(self, v):
		return self.g[v].keys()
	def get_neighbours_with_weight(self, v):
		return [(u, self.get_weight(v, u)) for u in self.g[v].keys()]
	def get_weight(self, u, v):
		return self.g[u][v]
	def is_inside(self, x, y):
		return x < self.nb_rows and x >= 0 and y < self.nb_columns and y >= 0
	def merge_paths(self, a, b, v):
		wa = self.get_weight(a, v)
		wb = self.get_weight(v, b)
		w = self.solve_path[wa][wb]
		if w:
			self.add_path(a, b, w)
	def parse_map(self, map_str):
		map = []
		for row in map_str.splitlines():
			map.append(row.split(' '))
		self.nb_rows = len(map)
		self.nb_columns = len(map[0])
		return map
	def print_all_nodes(self):
		for a in sorted(self.get_all_nodes()):
			print '%s -> %s' % (a, self.get_neighbours_with_weight(a))
