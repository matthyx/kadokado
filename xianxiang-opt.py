height = 6
width = 7

startMap =	[1,1,1,1,1,1,1
		,1,1,1,1,1,1,1
		,1,1,1,1,1,1,1
		,1,1,1,1,1,1,1
		,1,1,1,1,1,1,1
		,1,1,1,1,1,1,1]

def countDirChange(directions):
	changes = 0
	for i in range(len(directions)-1):
		if not directions[i] == directions[i+1]:
			changes = changes + 1
	return changes

class Board:
	def __init__(self, map, tuples=[], removed=set()):
		self.map = list(map)
		self.tuples = list(tuples)
		self.removed = frozenset(removed)
	def inBoard(self, xy):
		x, y = xy
		return x < height and x >= 0 and y < width and y >= 0
	def getCell(self, xy):
		x, y = xy
		return self.map[x * width + y]
	def setCell(self, xy, value):
		if self.inBoard(xy):
			x, y = xy
			self.map[x * width + y] = value
	def getPawns(self):
		pawns = set()
		for x in range(height):
			for y in range(width):
				if self.getCell((x,y)):
					pawns.add((x,y))
		return pawns
	def checkCell(self, xy, direction, previousCell):
		# create the list of moves to get here
		moves = []
		moves.extend(previousCell.movesHistory)
		moves.append(direction)
		if countDirChange(moves) < 2:
			# we are allowed to access it (less than 2 moves)
			if self.getCell(xy):
				# it's a pawn
				previousCell.startPawn.addPawn(xy, self.getCell(xy))
			else:
				# it's an empty cell
				if not xy in previousCell.visited:
					# we haven't already visited it
					# add to known empty cells
					previousCell.addVisited(xy)
					# create a new cell
					emptyCell = Cell(xy, 0, previousCell.visited, moves)
					emptyCell.startPawn = previousCell.startPawn
					# look for reachable cells from this one
					self.checkNeighbors(emptyCell)
	def checkNeighbors(self, cell):
		x = cell.x
		y = cell.y
		left  = (x, y - 1)
		right = (x, y + 1)
		up    = (x - 1, y)
		down  = (x + 1, y)
		if self.inBoard(left):
			self.checkCell(left, 'L', cell)
		if self.inBoard(right):
			self.checkCell(right, 'R', cell)
		if self.inBoard(up):
			self.checkCell(up, 'U', cell)
		if self.inBoard(down):
			self.checkCell(down, 'D', cell)
	def findPeers(self, pawnCoord):
		pawn = Cell(pawnCoord, self.getCell(pawnCoord))
		pawn.startPawn = pawn
		self.checkNeighbors(pawn)
		return pawn.peers

class Cell:
	def __init__(self, xy, value, visited=set(), movesHistory=[]):
		self.x, self.y = xy
		self.value = value
		self.peers = {}
		self.visited = set(visited)
		self.movesHistory = list(movesHistory)
	def coord(self):
		return (self.x,self.y)
	def addPawn(self, xy, value):
		if not xy == self.coord():
			newPawn = Cell(xy, value)
			self.peers[xy] = value
	def addVisited(self, xy):
		self.visited.add(xy)

knownMoves = {}
def findPossibleMoves(board):
	if knownMoves.has_key(str(board.map)):
		moves = knownMoves[str(board.map)]
	else:
		moves = set()
		for pawnCoord in board.getPawns():
			for peerCoord in board.findPeers(pawnCoord):
				# check if it's really a new move
				newMove = (pawnCoord,peerCoord)
				newMove2 = (peerCoord,pawnCoord)
				if not ((newMove in moves) or (newMove2 in moves)):
					moves.add((pawnCoord,peerCoord))
		knownMoves[str(board.map)] = moves
		print len(knownMoves)
	return moves

mapsPrinted = set()
def findMoves(board):
	boardsToLook = []
	boardsToLook.append(board)
	while len(boardsToLook):
		currentBoard = boardsToLook.pop()
		if len(currentBoard.getPawns()):
			for tuple in findPossibleMoves(currentBoard):
				(pawnCoord,peerCoord) = tuple
				newRemoved = set(currentBoard.removed)
				newRemoved.add("%s-%s" % (pawnCoord,peerCoord))
				# also add reverse tuple
				# sets are unordered
				newRemoved.add("%s-%s" % (peerCoord,pawnCoord))
				newTuples = list(currentBoard.tuples)
				newTuples.append("%s-->%s" % (pawnCoord,peerCoord))
				newBoard = Board(currentBoard.map, newTuples, newRemoved)
				newBoard.setCell(pawnCoord, 0)
				newBoard.setCell(peerCoord, 0)
				boardsToLook.append(newBoard)
#		else:
#			if currentBoard.removed not in mapsPrinted:
#				mapsPrinted.add(currentBoard.removed)
#				print currentBoard.tuples

if __name__ == "__main__":
	# Import Psyco if available
	try:
		import psyco
		psyco.full()
	except ImportError:
		pass
	startBoard = Board(startMap)
	findMoves(startBoard)

