import collections, copy, os, plateau, sys

try:
	import psyco
	psyco.full()
	print 'psyco optimized'
except ImportError:
	pass

with open(os.path.join(os.getcwd(), 'plateau3.txt')) as file:
	map_str = file.read()
p = plateau.Plateau(map_str)
print 'score theorique:', p.b.eval_board()

plateau_list = collections.deque()
plateau_list.append(p)

best_score = 0
while(plateau_list):
	cur_p = plateau_list.pop()
	moves = cur_p.g.get_all_moves()
	if not len(moves):
		if cur_p.score > best_score:
			print cur_p.score, cur_p.moves
			best_score = cur_p.score
		continue
	save_b = copy.copy(cur_p.b.b.map)
	best_plateaux = collections.deque()
	best_plateaux_score = 0
	for move in moves:
		(u, v) = move
		new_score = cur_p.b.remove(u, v) + cur_p.b.eval_board()
		cur_p.b.b.map = copy.copy(save_b)
		if best_plateaux_score > new_score:
			continue
		else:
			if new_score > best_plateaux_score:
				best_plateaux = collections.deque()
				best_plateaux_score = new_score
			new_p = copy.deepcopy(cur_p)
			move_score = new_p.b.remove(u, v)
			new_p.g.delete_node(u)
			new_p.g.delete_node(v)
			new_p.score += move_score
			new_p.moves.append((u, v, move_score))
			best_plateaux.append(new_p)
	plateau_list.extend(best_plateaux)
