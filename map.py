class Map:
	def __init__(self, map_str):
		self.map = []
		self.height = 0
		self.width = 0
		self.fill(map_str)
	def __iter__(self):
		return self.map.__iter__()
	def __str__(self):
		return '.'.join(sorted(self.map))
	def get(self, xy):
		m, n = xy
		x = int(m)
		y = int(n)
		return self.map[x * self.width + y]
	def fill(self, map_str):
		rows = map_str.splitlines()
		self.height = len(rows)
		self.width = len(rows[0].split(' '))
		for row in rows:
			self.map.extend(row.split(' '))
	def inside(self, xy):
		m, n = xy
		x = int(m)
		y = int(n)
		return x < self.height and x >= 0 and y < self.width and y >= 0
	def set(self, xy, value):
		if self.inside(xy):
			m, n = xy
			x = int(m)
			y = int(n)
			self.map[x * self.width + y] = value
