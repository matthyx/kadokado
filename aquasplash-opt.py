from copy import deepcopy

height = 6
width = 6

startMap =      [3,3,2,2,4,3
                ,1,3,2,2,2,4
                ,2,2,2,2,3,4
                ,2,2,2,2,1,3
                ,2,4,3,2,2,3
                ,3,4,2,1,2,3]

class Board:
	def __init__(self, map, moves=[]):
		self.map = list(map)
		self.moves = list(moves)
	def inBoard(self, xy):
		x, y = xy
		return x < height and x >= 0 and y < width and y >= 0
	def getCell(self, xy):
		x, y = xy
		return self.map[x * width + y]
	def setCell(self, xy, value):
		if self.inBoard(xy):
			x, y = xy
			self.map[x * width + y] = value
	def incrementCell(self, xy):
		value = self.getCell(xy) + 1
		if value > 4:
			# "pop"
			self.setCell(xy, 0)
			self.spitt(xy)
		else:
			self.setCell(xy, value)
	def bomb(self, xy):
		x, y = xy
		left  = (x, y - 1)
		right = (x, y + 1)
		up    = (x - 1, y)
		down  = (x + 1, y)
		self.setCell(left, 0)
		self.setCell(right, 0)
		self.setCell(up, 0)
		self.setCell(down, 0)
	def spitt(self, xy):
		x, y = xy
		# go left
		for y1 in range(y-1, -1, -1):
			if self.getCell((x,y1)):
				self.incrementCell((x,y1))
				break
		# go right
		for y1 in range(y+1, width, 1):
			if self.getCell((x,y1)):
				self.incrementCell((x,y1))
				break
		# go up
		for x1 in range(x-1, -1, -1):
			if self.getCell((x1,y)):
				self.incrementCell((x1,y))
				break
		# go down
		for x1 in range(x+1, width, 1):
			if self.getCell((x1,y)):
				self.incrementCell((x1,y))
				break
	def getBubbles(self):
		bubbles = set()
		emptyCells = set()
		for x in range(height):
			for y in range(width):
				if self.getCell((x,y)):
					bubbles.add((x,y))
		return bubbles
	def compareBubble(self, b1, b2):
		return self.getCell(b1) - self.getCell(b2)

def findMoves(board):
	boardsToLook = []
	boardsToLook.append(board)
	bestBoard = board
	nbMoves = 0
	while len(boardsToLook):
		currentBoard = boardsToLook.pop(0)
		if len(currentBoard.moves) > nbMoves:
			nbMoves = len(currentBoard.moves)
			print nbMoves
			if nbMoves > 4:
				break
		bubbles = currentBoard.getBubbles()
		if len(bubbles):
			for bubble in bubbles:
				if currentBoard.getCell(bubble) > 2:
					newBoard = Board(currentBoard.map, currentBoard.moves)
					newBoard.moves.append(str(bubble))
					newBoard.incrementCell(bubble)
					boardsToLook.append(newBoard)
		else:
			print len(currentBoard.moves)
			print currentBoard.moves
			break

if __name__ == "__main__":
	# Import Psyco if available
#	try:
#		import psyco
#		psyco.full()
#		print "Psyco optimized"
#	except ImportError:
#		pass
	startBoard = Board(startMap)
	findMoves(startBoard)

