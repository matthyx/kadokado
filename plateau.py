import board, graph

strl = """\
00 01 02 03 04 05
10 11 12 13 14 15
20 21 22 23 24 25
30 31 32 33 34 35
40 41 42 43 44 45
50 51 52 53 54 55
60 61 62 63 64 65"""

class Plateau:
	def __init__(self, map_str):
		self.b = board.Board(map_str)
		self.g = graph.Graph(strl)
		self.moves = []
		self.score = 0
