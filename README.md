# Synopsis
Set of script to solve some games from http://www.kadokado.com/

Many versions of the same script coexist, reflecting different iterations trying to optimize

The most interesting one is probably xianxiang-opt.py using graphs to manipulate solution space

# Dependencies
* python 2.7
